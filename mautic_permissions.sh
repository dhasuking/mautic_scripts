#!/bin/bash
user=$1;
find . -type f -exec chmod 755 {} \;
find . -type f -exec chmod 644 {} \;
chmod -R g+w app/cache/;
chmod -R g+w app/logs/;
chmod -R g+w app/config/;
chmod -R g+w media/files/;
chmod -R g+w media/images/;
chmod -R g+w translations/;
chown -R $user:www-data .;
